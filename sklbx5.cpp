﻿// sklbx5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

struct item {
    item() {
        value = 0;
        prev = NULL;
    }
    int value;
    item* prev;
};

class Stack {
public:
    Stack(){
        last = NULL;
        first = NULL;
    }

    void Push(const int& new_value) {
        item* newitem = new item[1];
        newitem->prev = last;
        newitem->value = new_value;
        last = newitem;
        cout <<new_value<< " pushed succesfully" << endl;
    }

    void Print() {
        item* tmp = last;
        if (!tmp)
            cout << "Empty stack\n";
        else
        {
            cout << "Items in the stack:\n";
            while (tmp) {
                cout << tmp->value << " ";
                tmp = tmp->prev;
            }
            cout << endl;
        }
    }
    void Pop() {
        if (last) {
            cout << "Last pushed item: " << last->value << endl;
        }
        else cout << "Empty stack\n";
    }

    void FindItem(const int& value) {
        item* tmp = last;
        while (tmp) {
            if (tmp->value == value) {
                cout << value << " is in the stack\n";
                return;
            }
            tmp = tmp->prev;
        }
        cout << value << " isn't in the stack\n";
    }

private:
    item* last;
    item* first;
};


int main()
{
    Stack st;
    st.Print();
    st.Pop();
    st.Push(1);
    st.Push(2);
    st.Push(44);
    st.Print();
    st.Pop();
    st.FindItem(2);
    st.FindItem(5);

}
